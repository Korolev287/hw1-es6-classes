// JavaScript — прототипно-ориентированный язык.
// В прототипном наследовании классы не используются. 
// Вместо этого объекты создаются из других объектов. 
// Мы начинаем с обобщённого объекта — прототипа, который можно 
// использовать для создания других объектов путём его клонирования 
// или расширять его разными функциями.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return `${this._name}`
    }
    set name(value){
        this._name = value;
    }
    get age() {
        return `${this._age}`
    }
    set age (value) {
        if (value < 18) { 
          alert(`Работник ${this._name} слишком юн`);
          return;
        }
        this._age = value;
      }
    get salary() {
        return this._salary
    }
    set salary (value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary)
      this.lang = lang;
     }
     
     set salary (value) {
        this._salary = value;
    }

     get salary () {
         return this._salary * 3;
     }
}

const maria = new Programmer('Мария', 25, 20000, ['Французкий, Польский'])
console.log(maria);
console.log(maria.salary);

const andrey = new Programmer('Андрей', 35, 27000, 'Испанский')
console.log(andrey);
console.log(andrey.salary);

const sasha = new Programmer('Саша', 35, 13000, 'Португальский')
console.log(sasha);
console.log(sasha.salary);
